﻿using UnityEngine;
using System.Collections;

public  class LifeBossController : MonoBehaviour {
	public  GameObject lifeBossBarra;
	public  float larguraLife;
	// Use this for initialization
	void Start () {
		larguraLife = lifeBossBarra.transform.localScale.x;
	}
	
	// Update is called once per frame
	public  void diminuiBarraLifeBoss(){
		transform.localScale -= new Vector3(0.1f, 0, 0);
	}

}
