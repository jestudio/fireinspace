﻿using UnityEngine;
using System.Collections;


public class BossScript : MonoBehaviour {

	public float moveSpeed = 1.4f;  //per second
	public  int computerDirection;
	private Vector3 moveDirection = new Vector3(-2.52f, 0, 0);
	private bool movingLeft = false;
	public static bool chamou;
	public GameObject bala;
	public GameObject localBala;
	public static int lifeBoss;
	GameObject chatBoss;
	bool mostraChat = true;


	void Start(){
		chamou 			= false;
		lifeBoss		= Random.Range(45,60);
		chatBoss		= GameObject.Find("ChatBoss").gameObject;


	}


	void Update () 
	{ 


		if(!chamou){
			mostraChat=true;
		}

		if(lifeBoss == 0){
			GetComponent<AudioSource>().Play();
			ChamaMorteBoss();
		}

		if(chamou){

			if(mostraChat){
				chamaChatInicio();
			}

			if(transform.position.y > 3.5f){
				transform.Translate(Vector3.down * Time.deltaTime);
			}

				

				StartCoroutine(removeChat());





		// If thief is moving to the right and its position is -101
		// set movingLeft to true and change direction
		if(!movingLeft && transform.localPosition.x <= -2.52f)
		{
			moveDirection = new Vector3(2.52f,0,0);
			movingLeft = true;
		}
		
		// If thief is moving to the left and its position is 126
		// set movingLeft to false and change direction  
		if(movingLeft && transform.localPosition.x >= 2.52f)
		{
				moveDirection = new Vector3(-2.52f,0,0);
				movingLeft = false;
		}
		
		transform.Translate(Random.Range(1.0f,2.5f) * Time.deltaTime * moveDirection);
		
		} 
	}

	void balaBoss(){
		Instantiate(bala, new Vector3(
				localBala.transform.position.x, 
				localBala.transform.position.y, 
				localBala.transform.position.z), 
		            Quaternion.identity);


		Instantiate(bala, new Vector3(
			Random.Range(localBala.transform.position.x-1.5f,localBala.transform.position.x+1.5f) , 
			Random.Range(localBala.transform.position.y-0.1f,localBala.transform.position.y+0.1f), 
			localBala.transform.position.z), 
		            Quaternion.identity);

	}

	void OnTriggerEnter2D(Collider2D coll){
		if(coll.name=="LocalCollider"){
			InvokeRepeating ("balaBoss",  2f, 1.0f);
		}
	}


	void ChamaMorteBoss(){
		transform.position 		= new Vector3(-1.26f, 7f, -0.1f);
		chamou = false;
		lifeBoss				= Random.Range(40,60);
		Inimigos.chamaInimigos 	= true;
		CancelInvoke();
	}


	public void chamaChatInicio(){
		
		chatBoss.transform.position = new Vector3(transform.position.x-1,transform.position.y-1.2f,transform.position.z);
		
		
	}

	 IEnumerator removeChat(){
		yield return new WaitForSeconds(4f);
		chatBoss.transform.position = new Vector3(transform.position.x-7,transform.position.y,transform.position.z);
		mostraChat = false;
	}



	}