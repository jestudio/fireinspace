﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SocialPlatforms;

public class GameOverController : MonoBehaviour {

	private int bestScore;
	private int score;
	public Text bestScoreText;
	public Text scoreText;
	
	// Use this for initialization
	void Start () {
		score = PlayerPrefs.GetInt ("score");
		bestScore = PlayerPrefs.GetInt ("bestScore");
		bestScoreText.text 	= "BEST SCORE: " + bestScore.ToString();
		scoreText.text 	= "SCORE: " + score.ToString();
//		Ads.RequestBanner();

		if(Social.localUser.authenticated){
			GameController.fazConquista(score);
			GameController.sharePontosBest();
		}



		Personagem.pontos = 0;
		Personagem.countPontosBoss = Random.Range(35,50);

	}

	void Update(){
		if (Input.GetKey("escape"))
			Application.LoadLevel("home");

	}
}
