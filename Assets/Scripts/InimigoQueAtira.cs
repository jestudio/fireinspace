﻿using UnityEngine;
using System.Collections;

public class InimigoQueAtira : MonoBehaviour {
	public GameObject bala;
	public GameObject localBala;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("balaInimigo",  3f, 4f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void balaInimigo(){
		Instantiate(bala, new Vector3(
			localBala.transform.position.x, 
			localBala.transform.position.y, 
			localBala.transform.position.z), 
		            Quaternion.identity);
	}
}
