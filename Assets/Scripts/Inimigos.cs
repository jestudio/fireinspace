﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Inimigos : MonoBehaviour {

	public GameObject[]	prefabInimigo;
	private float 		speedRespaw;
	private float 		speedRespawTime;
	public static bool 	chamaInimigos;
	public static int 	inimigoIndex;
	public Text			levelFase;
	public static int 			numDificuldade;

	// Use this for initialization
	void Start () {
		numDificuldade = 1;
		chamaInimigos = true;
		if(chamaInimigos)
		InvokeRepeating ("Spawn",  GameController.speedRespawEnimy, GameController.speedRespawTimeEnimy);

	}

	void Update(){

		levelFase.text = "LV " + Time.time;

		if(Personagem.pontos >= GameController.dificuldade ){
				numDificuldade++;
				GameController.speedRespawEnimy -= -0.12f;
				GameController.speedRespawTimeEnimy -= 0.12f;
				GameController.dificuldade = (Personagem.pontos + ((Personagem.pontos*70)/100));
				CancelInvoke();
				InvokeRepeating ("Spawn",  GameController.speedRespawEnimy, GameController.speedRespawTimeEnimy);

		}


		levelFase.text = "LV " + numDificuldade.ToString();

		if (transform.position.y < -5.5f) {
			Destroy(prefabInimigo[inimigoIndex].gameObject);
			inimigoIndex = Random.Range(0, prefabInimigo.Length);
		}
	}

	void Spawn(){
		if(chamaInimigos){
			Instantiate(prefabInimigo[inimigoIndex],new Vector3(Random.Range(-2.34f, 2.34f),6,-0.1f),transform.rotation);
			inimigoIndex = Random.Range(0, prefabInimigo.Length);
		}
	}




}
