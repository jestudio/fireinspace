﻿using UnityEngine;
using System.Collections;

public class MoveOffSet : MonoBehaviour {

	public float 	speed;
	public Material material;
	private float 	offSet;

	// Use this for initialization
	void Start () {
		material = GetComponent<Renderer> ().material;
	}
	
	// Update is called once per frame
	void Update () {
		offSet += 0.001f;
		material.SetTextureOffset("_MainTex", new Vector2 (0,offSet * speed));


	}
}
