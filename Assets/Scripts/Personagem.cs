﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SocialPlatforms;

public class Personagem : MonoBehaviour {

	public GameObject 	localSaidaBala;
	public GameObject 	localSaidaBala2;
	public GameObject 	localSaidaBala3;
	public GameObject 	personagem;
	public GameObject 	bala;
	public float 		speedMovimento;
	private bool 		disparada;
	public Text			pontuacaoText;
	public Text			lifePersonagem;
	public static int 	pontos = 0;
	public AudioClip	somMorte;
	public static float countPontosBoss;
	public static bool specialAtack;
	public static bool quickleBala 		= false;
	public static bool chamouBalaRapida;

	public static bool chamaStrartAgain = false;
	private Animator playerController;
	public static float speedBala;
	public GameObject varredura;
	public static bool chamaVarredura = false;
	private bool tocouVarredura = false;
	public static int vidasPersonagem;
	GameObject chatEnimigo;


	// Use this for initialization
	void Start () {
		playerController = GetComponent<Animator>();
		chatEnimigo = GameObject.Find("Chat").gameObject;
		countPontosBoss = Random.Range(40f,70f);

		specialAtack     = false;

		if(pontos == 0){
			speedBala = 1f;
			//countPontosBoss = Random.Range(40f,70f);
			vidasPersonagem = 3;
		}

		lifePersonagem.text = "";
		for(int x=0; x < vidasPersonagem; x++){
			lifePersonagem.text += "♥";
		}



		if(!quickleBala){
			chamouBalaRapida = true;
		}


		InvokeRepeating ("Spawn",  speedBala, speedBala);
	}
	
	// Update is called once per frame
	void Update () {

		if(Time.time > 5f){
			chatEnimigo.transform.position = new Vector3(personagem.transform.position.x-7,personagem.transform.position.y+1.2f,personagem.transform.position.z);
		}else{
			chamaChatInicio();
		}

		if(quickleBala && chamouBalaRapida){
			 	speedBala 		 = 0.2f;
				chamouBalaRapida = false;
				Start();

		}

			
		if(chamaStrartAgain){
			CancelInvoke();
			speedBala			= 1f;
			chamaStrartAgain 	= false;
			quickleBala 		= false;
			Start();
		}


		if(chamaVarredura){
			//VarreduraScript.audioVarredura.volume = 1f;
			if(tocouVarredura){
				tocouVarredura =false;
			}


			varredura.transform.Translate(Vector3.up * 2.5f * Time.deltaTime);
			if( varredura.transform.position.y > 5.22f ){
				chamaVarredura=false;
				varredura.transform.position = new Vector3(0.01f,-5.45f,-0.03f);

			}
		}
			

		//TOTAL PONTUACAO
		pontuacaoText.text 	= pontos.ToString();

		if (Input.GetKey("escape"))
			Application.LoadLevel("home");



		playerController.SetBool("SpeciaAtack",specialAtack);


		if(pontos >= countPontosBoss){
				BossScript.chamou 		= true;
				Inimigos.chamaInimigos 	= false;
		}



			//float speed = Input.acceleration.x;
			float speed = Input.GetAxisRaw("Horizontal");

			if(speed < 0){
				speed = -speed;
			}

			speed = speed * speedMovimento;
			
			if(Input.acceleration.x > 0 || Input.GetAxis("Horizontal") > 0 ){
				if(personagem.transform.position.x < 2.26f){
					personagem.transform.Translate(Vector2.right * speed  * Time.deltaTime);
				}
			}


			if (Input.acceleration.x < 0 || Input.GetAxis("Horizontal") < 0) {
				if(personagem.transform.position.x > -2.26f){
					personagem.transform.Translate(Vector2.left * speed * Time.deltaTime);
				}
			}




			bala.transform.Translate(Vector2.up * 3.0f * Time.deltaTime);

		if(bala.transform.position.x > 3){
			bala.transform.position = localSaidaBala.transform.position;
		}

		/*
			pontuacaoText.text = "DIF:" + GameController.dificuldade.ToString() + "- TIS: "+
			GameController.speedRespawTimeEnimy.ToString() + " - TISW:" +
				GameController.speedRespawEnimy.ToString();
		*/
		//DIFICULDADE DO JOGO


	}

	void Spawn(){
		Instantiate(bala,new Vector3(localSaidaBala.transform.position.x,localSaidaBala.transform.position.y,localSaidaBala.transform.position.z), localSaidaBala.transform.rotation);
		if(specialAtack){
			Instantiate(bala,new Vector3(localSaidaBala2.transform.position.x,localSaidaBala2.transform.position.y,localSaidaBala2.transform.position.z), localSaidaBala2.transform.rotation);
			Instantiate(bala,new Vector3(localSaidaBala3.transform.position.x,localSaidaBala3.transform.position.y,localSaidaBala3.transform.position.z), localSaidaBala3.transform.rotation);
		}

	}


	void OnTriggerEnter2D(Collider2D coll){
		if(coll.tag == "Special"){
			coll.gameObject.transform.position = new Vector3(Random.Range(-2.52f,2.52f),Random.Range(40,70),transform.position.z);


			float tempPowerUp = Random.Range(0,18);
			//float tempPowerUp = 50;
			if(tempPowerUp < 5){
			specialAtack = true;
			}

			if(tempPowerUp >= 5 && tempPowerUp < 10 ){
			quickleBala  = true;
			}

			if(tempPowerUp >= 10 && tempPowerUp < 16){
				//Varredura
				chamaVarredura = true;
				tocouVarredura = true;
				VarreduraScript.playVarredura();
			}

			if(tempPowerUp >= 16){
				if(vidasPersonagem < 3 ){
					vidasPersonagem++;
					aumentaVidaPersonagem();
				}else{
					quickleBala  = true;
				}

			}


				


		}


		if(coll.tag == "Boss" || coll.tag=="Inimigos" || coll.tag == "BalaBoss"){
			vidasPersonagem--;
//			Handheld.Vibrate();
			GetComponent<AudioSource>().Play();
			Destroy(coll.gameObject);
			aumentaVidaPersonagem();
		}

		if(vidasPersonagem == 0 ){
			GetComponent<AudioSource>().Play();
			StartCoroutine(chamaGameOver ());
		}
	}


	IEnumerator chamaGameOver(){

		yield return new WaitForSeconds(0.4f);
		PlayerPrefs.SetInt ("score",pontos);
		if(pontos > PlayerPrefs.GetInt("bestScore") ){
			PlayerPrefs.SetInt ("bestScore",pontos);

			if(Social.localUser.authenticated){
				GameController.sharePontosBest(); //SHARE SCORE
			}

		}
		
		Application.LoadLevel ("gameOver");
	}

	void aumentaVidaPersonagem(){
		lifePersonagem.text = "";
		for(int x=0; x < vidasPersonagem; x++){
			lifePersonagem.text += "♥";
		}
	}

	public void chamaChatInicio(){

		chatEnimigo.transform.position = new Vector3(personagem.transform.position.x+1,personagem.transform.position.y+1.2f,personagem.transform.position.z);
		

	}

}
