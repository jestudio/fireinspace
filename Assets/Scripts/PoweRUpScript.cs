﻿using UnityEngine;
using System.Collections;

public class PoweRUpScript : MonoBehaviour {
	public Transform poweUp;

	// Use this for initialization
	void Start () {
		transform.position = new Vector3(Random.Range(-2.52f,2.52f),Random.Range(40,60),transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {

		transform.Rotate(Vector3.forward * 600 * Time.deltaTime, Space.Self);
		transform.position = new Vector3 (transform.position.x, transform.position.y-0.027f * Time.deltaTime,transform.position.z);

		if(transform.position.y < -5.62f){
			transform.position = new Vector3(Random.Range(-2.52f,2.52f),Random.Range(50,70),transform.position.z);
		}

	}
}
