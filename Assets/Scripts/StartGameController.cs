﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartGameController : MonoBehaviour {
	private int bestScore;
	public Text bestScoreText;

	// Use this for initialization
	void Start () {
		bestScore = PlayerPrefs.GetInt ("bestScore");
		bestScoreText.text 	= "BEST SCORE: " + bestScore.ToString ();
		Personagem.pontos 	= 0;
//		Ads.RequestBanner(); 
	}

	void Update(){
		if (Input.GetKey("escape"))
			Application.Quit();

	}

}
