﻿using UnityEngine;
using System.Collections;

public class VarreduraScript : MonoBehaviour {

	public static AudioSource audioVarredura;
	// Use this for initialization
	void Start () {
		audioVarredura = GetComponent<AudioSource>();
	}


	public static void playVarredura(){
		audioVarredura.Play();
	}


	void OnTriggerEnter2D(Collider2D coll){

		if(coll.tag == "BalaBoss"){
			//Personagem.pontos++;
			Destroy(coll.gameObject);
		}

		if(coll.tag == "Inimigos"){
			Personagem.pontos++;
			Destroy(coll.gameObject);
		}

		if(coll.tag == "Boss"){
			//CHEFE IMUNE
		}

	}

}
