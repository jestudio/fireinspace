﻿using UnityEngine;
using System.Collections;

public class balaScript : MonoBehaviour {

	public AudioClip	somDestroy;
	public GameObject 	lifeBarraChefe;
	private float		removeLife = 0f;

	// Use this for initialization
	void Start () {
		GetComponent<AudioSource>().Play();
		lifeBarraChefe = GameObject.FindWithTag("LifeBoss").gameObject;
		if(removeLife ==0 ){
			removeLife = (lifeBarraChefe.transform.localScale.x / BossScript.lifeBoss);
		}
	}
	

	void Update(){
		transform.Translate (Vector3.up * 10.0f * Time.deltaTime);
		if(transform.position.y > 4.9f ){
			Destroy(gameObject);
		}
	}


	void OnTriggerEnter2D(Collider2D coll){
		if(coll.tag == "Boss"){
			Destroy (this.gameObject);

				BossScript.lifeBoss--;
	

			Personagem.pontos++;
			lifeBarraChefe.transform.localScale -= new Vector3(removeLife , 0, 0);

			if(BossScript.lifeBoss == 0){
				lifeBarraChefe.transform.localScale += new Vector3(6f , 0, 0);
				Personagem.pontos 				+= 75;
				Personagem.countPontosBoss 		= Random.Range(Personagem.pontos , Personagem.pontos + Random.Range(100,200));
				Inimigos.chamaInimigos 			= true;
				//VEJA A VARREDURASCRIPT POIS ELE MATA BOSS
			}
		}

		if(coll.tag == "BalaBoss"){
			Destroy (coll.gameObject);
			Personagem.pontos +=2;
		}

		if(coll.tag == "Inimigos"){
			AudioSource.PlayClipAtPoint(somDestroy, Camera.main.transform.position);
			if(Inimigos.numDificuldade < 9){
				Personagem.pontos++;
			}else{
				Personagem.pontos +=2;
			}

			Destroy (coll.gameObject);
			Destroy (this.gameObject);
		}
	}
	



}
